# Kefir

Il est question ici du kefir de fruits. Cette recette est largement inspirée de https://www.symbiose-kefir.fr/recette-kefir-de-fruits/

## Matériel

- Graines de kefir
- Un récipient transpartent d'au moins 1 litre
- Une bouteille vide genre limonade

## Ingrédients

- 750 mL d'eau
- 30 g de sucre
- 30 g de graines de kéfir
- au moins 14 g de fruits secs
- au moins 1 tranche de citron

## Préparation du kéfir

Dans un bocal d'un litre (j'utilise un bocal style 'le parfait'), mettre 750ml d'eau.

Ajouter 30 g de sucre et mélanger avec un cuillère jusqu'à ce que le sucre soit fondu.

Ajouter 30 g de graines de kéfir 'égoutées' (je les prends avec la même cuillère en enlevant l'eau à la grosse).

Ajouter les autres ingrédients.

Fermer le couvercle non hermétiquement (n'utilisez pas le caotchouc si vous utilisé un bocal).

Attendre que les fruits secs remontent à la surface (idéalement à 20-25 degrés) ou plus si vous aimez bien fermenté (nous on attends 36h).

Filtrer et transférer dans la bouteille de limonade que vous fermez et laisser à température pendant le même temps. On laisse aussi 36h parce qu'on aime les bulles.

Récupérer les graines de kéfir dans votre passoire. Mettez-les au frigo dans de l'eau en attendant le prochain kéfir.

Chez moi les graines doublent en 36h du coup je suis obligée d'en jeter pas mal au composte...
