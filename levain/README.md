# Levain

Dans ce dossier toutes les recettes qui utilisent notre levain.

Comme tous les levains naturels, notre levain est unique et je ne sais pas à quel point ce qu'on fait est transposable à d'autres levains.

## Conservation du levain

Nous gardons toujours notre levain au frigo. Si il commence à il y avoir de "l'eau" à la surface c'est qu'il a faim. Il faut le nourrir.

### En cas d'absence

Si on part moins d'une semaine, on nourrit le levain avant de partir et on le met au frigo pendant la semaine.

Si on part plus longtemps, on le nourrit, on le pèse. Quand il est au maximum, on l'étale sur du papier sulfurisé le plus finement possible et on attend qu'il sèche (ou on le met au four à 50 degrés). On stocke ensuite les 'paillettes' au congélateur. Au retour des vacances, on rermet les paillettes dans un récipient et on complète avec de l'eau pour revenir au même poids. Le lendemain on le nourrit. En général, la première levée est très lente mais ensuite il repart comme avant.

## Nourrir le levain

C'est très simple, on mélange le levain qui était au frigo avec x g de farine de blé avec x g de farine de seigle avec 2x g d'eau (en théorie il vaut mieux utiliser de l'eau dont le chlore s'est évaporé mais chez nous l'eau n'est pas très chlorée alors on fait avec l'eau du robinet). Il vaut mieux nourrir le levain au moins autant que son poids (par exemple nourrir 50g de levain avec au moins 13g de chaque farine et 26g d'eau). Je sais pas si il y a une limite dans l'autre sens mais ça nous arrive régulièrement de nourrir 100g de levain avec 100g de chaque farine et 200g d'eau.

Quand la préparation est homogène, si c'est pour faire une recette, on met la quantité de levain nécessaire à la recette dans un saladier et on laisse le reste dans le récipient. On couvre les 2 et on attends 4-5h que le levain lève, qu'il soit bombé mais qu'il y ait des bulles à la surface.

Quand c'est le cas, on prépare la recette et on remet le levain dans le récipient au frigo. On ne nourrit pas notre levain à moins de 24h d'intervalle parce qu'on a remarqué qu'il lève moins bien.

## Recettes

- [Brioche](./brioche.md)
- [Pain cocotte](./pain.md)
- [Pizza](./pizza.md)
