# Pain

## Ingrédients

Levain | 100g | 150g | 200g
 --- | --- | --- | ---
Eau | 267g | 400g | 533g
Farine (T55-T65) | 433g | 650g | 867g
Sel | 8g | 12g | 16g

## Recette

Mélanger les ingrédients.

Pétrir pendant 5 minutes avec les crochets d'un batteur.

Laisser reposer 1h à température ambiante (quand il fait chaud on zappe cette étape).

Laisser au frigo une nuit à 5 jours ou dégazer et attendre 2h à température.

Quand on le sort du frigo, je dégaze dans le saladier. Je le laisse à peu près 1h à température couvert.

Façonner le pain sur un plan de travail fariné (faire une boule couverte de farine).

Mettre le four en chaleur tournante à 250 degrés avec la cocotte dedans (laisser le pain reposer pendant ce temps soit 20-30 minutes).

Quand le four est chaud, sortir la cocotte du four (! aux risques de brûlures le couvercle est chaud). Mettre le pain dedans. Grigner le pain. Refermer la cocotte et enfourner la cocotte pendant 45 minutes à 230 degrés.

Retirer le couvercle de la cocotte et laisser griller pendant encore 15 minutes.

Laisser refroidir sur une grille.

Régalez-vous
