# Pizza

## Ingrédients

Levain | 100g | 150g
 --- | --- | ---
Eau | 120g | 180g
Farine (T55-T65) | 300g | 450g
Sel | 1/3cac | 2/3cac
Huile d'olive | 2cas | 3cas

## Recette

Mélanger les ingrédients.

Pétrir pendant 5 minutes avec les crochets d'un batteur.

Laisser reposer 1h à température ambiante

Laisser au frigo 24h

Étaler au rouleau, garnir, faire cuire au four.

Régalez-vous
