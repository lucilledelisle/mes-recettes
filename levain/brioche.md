# Brioche

## Ingrédients

Levain | 100g | 200g
 --- | --- | ---
Farine (T55-T65) | 300g | 600g
Sucre | 30g | 60g
Oeufs | 2 | 4
Lait | 50g | 100g
Beurre | 75g | 150g

## Recette

Mélanger les ingrédients.

Pétrir pendant 5 minutes avec les crochets d'un batteur.

Laisser reposer 1h à température ambiante

Optionnel: Laisser au frigo quelques heures à 5 jours

Mettre la pâte sur un plan de travail fariné

Séparer la pâte en 3 parties égales.

Pour chaque morceau, étaler avec la main et plier en 3, réétaler et plier en 3 dans l'autre sens 3-10 fois.

Faire 3 boudins et faire une tresse.

Mettre dans un plat à cake beurré.

Mettre dans le four froid toute la nuit.

Programmer le four pour 30 minutes à 180 degrés (chez nous c'est le programme sole pulsé) pour qu'elle soit chaude quand vous vous réveillez.

Régalez-vous
